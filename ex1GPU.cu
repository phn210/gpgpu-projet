#include <vector>
#include <cstdint>
#include <cmath>

__global__ void GPU_naive_viewtest(
    const int width,
    const int height,
    const int x_c,
    const int y_c,
    uint8_t *const pixel_in,
    uint8_t *const pixel_out
) {
    // Calculate the overall index of thread in the grid
    int tid = threadIdx.x + blockIdx.x * blockDim.x;

    // Calculate height map value of pixel c
    int z_c = pixel_in[ x_c + y_c * width];

    // 
    while (tid < width * height) {
        // The center pixel must be visible itself
        if (tid == (x_c + y_c * width)) {
            pixel_out[tid] = 255;
            tid += blockDim.x * gridDim.x; 
            continue;
        }

        // Calculate coordination of examining pixel - p
        int x_p = tid % width;
        int y_p = tid / width;
        int z_p = pixel_in[tid];

        // Caculate angle from point c to pixel p
        float angle_p = atan((z_p - z_c)
                    /sqrt(pow(x_p - x_c, 2) + pow(y_p - y_c, 2)));


        // Find points for visibility check with DDA
        float dx = x_p - x_c;
        float dy = y_p - y_c;
        int step = abs(dx) >= abs(dy) ? abs(dx) : abs(dy);

        float xIncrement = dx / step;
        float yIncrement = dy / step;

        int isVisible = 255;
        int j = 1;

        while (j < step) {
            // Calculate coordination of examining pixel
            int x = x_c + j * xIncrement;
            int y = y_c + j * yIncrement;
            int z = pixel_in[ x + y * width ];

            // Calculate angle from pixel c to this pixel
            float angle = atan((z - z_c)
                    /sqrt(pow(x - x_c, 2) + pow(y - y_c, 2)));
            
            // Check visibility of pixel p
            if (angle >= angle_p) {
                isVisible = 0;
                break;
            }

            j++;
        }

        pixel_out[tid] = isVisible;

        // Jump to the next tid value
        tid += blockDim.x * gridDim.x; 
    }
}

__global__ void GPU_optimized_viewtest_v1_part_1(
    const int width,
    const int height,
    const int x_c,
    const int y_c,
    uint8_t *const pixel_in,
    float *const angle
) {
    // Calculate the overall index of thread in the grid
    int tid = threadIdx.x + blockIdx.x * blockDim.x;

    // Calculate height map value of pixel c
    int z_c = pixel_in[ x_c + y_c * width ];

    // 
    while (tid < height * width) {
        // The center pixel must be visible itself
        if (tid == (x_c + y_c * width)) {
            angle[tid] = 0;
            tid += blockDim.x * gridDim.x; 
            continue;
        }

        // Calculate coordination of examining pixel - p
        int x_p = tid % width;
        int y_p = tid / width;
        int z_p = pixel_in[tid];

        // Caculate angle from point c to pixel p
        angle[tid] = atan((z_p - z_c)
                    /sqrt(pow(x_p - x_c, 2) + pow(y_p - y_c, 2)));

        tid += blockDim.x * gridDim.x;
    }
}

__global__ void GPU_optimized_viewtest_v1_part_2(
    const int width,
    const int height,
    const int x_c,
    const int y_c,
    uint8_t *const pixel_in,
    uint8_t *const pixel_out,
    float *const angle
) {
    // Calculate the overall index of thread in the grid
    int tid = threadIdx.x + blockIdx.x * blockDim.x;

    while (tid < height * width) {
        // The center pixel must be visible itself
        if (tid == (x_c + y_c * width)) {
            pixel_out[tid] = 255;
            tid += blockDim.x * gridDim.x; 
            continue;
        }

        float angle_p = angle[tid];

        // Calculate coordination of examining pixel - p
        int x_p = tid % width;
        int y_p = tid / width;

        // Find points for visibility check with DDA
        float dx = x_p - x_c;
        float dy = y_p - y_c;
        int step = abs(dx) >= abs(dy) ? abs(dx) : abs(dy);

        float xIncrement = dx / step;
        float yIncrement = dy / step;

        int isVisible = 255;
        int j = 1;

        while (j < step) {
            // Calculate coordination of examining pixel
            int x = x_c + j * xIncrement;
            int y = y_c + j * yIncrement;
            
            // Check visibility of pixel p
            if (angle[ x + y * width ] >= angle_p ) {
                isVisible = 0;
                break;
            }

            j++;
        }

        pixel_out[tid] = isVisible;

        // Jump to the next tid value
        tid += blockDim.x * gridDim.x; 
    }
}

__global__ void GPU_optimized_viewtest_v2(
    const int width,
    const int height,
    const int x_c,
    const int y_c,
    uint8_t *const pixel_in,
    uint8_t *const pixel_out,
    float *const angles
) {
    // Calculate the overall index of thread in the grid
    int tid = threadIdx.x + blockIdx.x * blockDim.x;

    // Calculate height map value of pixel c
    int z_c = pixel_in[ x_c + y_c * width ];

    // 
    while (tid < width * height) {
        // The center pixel must be visible itself
        if (tid == (x_c + y_c * width)) {
            pixel_out[tid] = 255;
            tid += blockDim.x * gridDim.x; 
            continue;
        }

        // Calculate coordination of examining pixel - p
        int x_p = tid % width;
        int y_p = tid / width;
        int z_p = pixel_in[tid];

        // Caculate angle from point c to pixel p
        float angle_p = atan((z_p - z_c)
                    /sqrt(pow(x_p - x_c, 2) + pow(y_p - y_c, 2)));
        angles[tid] = angle_p;

        // Find points for visibility check with DDA
        float dx = x_p - x_c;
        float dy = y_p - y_c;
        int step = abs(dx) >= abs(dy) ? abs(dx) : abs(dy);

        float xIncrement = dx / step;
        float yIncrement = dy / step;

        int isVisible = 255;
        int j = 1;

        while (j < step) {
            // Calculate coordination of examining pixel
            int x = x_c + j * xIncrement;
            int y = y_c + j * yIncrement;
            int z = pixel_in[ x + y * width ];

            // Calculate angle from pixel c to this pixel
            float angle = angles[x + y * width];
            if (angle == 0) {
                angle = atan((z - z_c)
                    /sqrt(pow(x - x_c, 2) + pow(y - y_c, 2)));
                // atomicExch(&angles[x + y * width], angle);
                angles[x + y * width] = angle;
            }
            
            // Check visibility of pixel p
            if (angle >= angle_p) {
                isVisible = 0;
                break;
            }

            j++;
        }

        pixel_out[tid] = isVisible;

        // Jump to the next tid value
        tid += blockDim.x * gridDim.x; 
    } 
}