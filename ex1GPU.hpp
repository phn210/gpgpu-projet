#ifndef __EX1GPU_HPP__
#define __EX1GPU_HPP__

__global__ void GPU_naive_viewtest(
    const int width,
    const int height,
    const int x_c,
    const int y_c,
    uint8_t *const pixel_in,
    uint8_t *const pixel_out
);

__global__ void GPU_optimized_viewtest_v1_part_1(
    const int width,
    const int height,
    const int x_c,
    const int y_c,
    uint8_t *const pixel_in,
    float *const angle
);

__global__ void GPU_optimized_viewtest_v1_part_2(
    const int width,
    const int height,
    const int x_c,
    const int y_c,
    uint8_t *const pixel_in,
    uint8_t *const pixel_out,
    float *const angle
);

__global__ void GPU_optimized_viewtest_v2(
    const int width,
    const int height,
    const int x_c,
    const int y_c,
    uint8_t *const pixel_in,
    uint8_t *const pixel_out,
    float *const angles
);

#endif // __EX1GPU_HPP__