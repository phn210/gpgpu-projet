#ifndef __EX2GPU_HPP__
#define __EX2GPU_HPP__

__device__ void atomicMaxUint8(uint8_t *address, uint8_t val);

__global__ void GPU_naive_tiled(
    const int width,
    const int height,
    const int finalWidth,
    const int finalHeight,
    const int tileWidth,
    const int tileHeight,
    uint8_t *const pixel_in,
    uint8_t *const pixel_out
);

__global__ void GPU_optimized_tiled(
    const int width,
    const int height,
    const int finalWidth,
    const int finalHeight,
    const int tileWidth,
    const int tileHeight,
    uint8_t *const pixel_in,
    uint8_t *const pixel_out
);


#endif // __EX2GPU_HPP__