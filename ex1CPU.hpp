#ifndef __EX1CPU_HPP__
#define __EX1CPU_HPP__

void CPU_viewtest(
    const int width,
    const int height,
    const int x_c,
    const int y_c,
    uint8_t *const pixel_in,
    uint8_t *const pixel_out
);

#endif // __EX1CPU_HPP__