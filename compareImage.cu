#include <iostream>
#include "utils/ppm.hpp"

void printUsage() 
{
	std::cerr	<< "Usage: " << std::endl
			<< " \t -f1 <file_name_1> -f2 <file_name_2>" 			
		    << std::endl << std::endl;
	exit( EXIT_FAILURE );
}

int main( int argc, char **argv) {
    // Load image
    char fileName1[2048];
	char fileName2[2048];

	// Parse program arguments
	if ( argc == 0 ) 
	{
		std::cerr << "Please give a file..." << std::endl;
		printUsage();
	}

	for ( int i = 1; i < argc; ++i ) 
	{
		if ( strcmp( argv[i], "-f1" ) ) 
		{
			if ( sscanf( argv[++i], "%s", &fileName1 ) != 1 )
				printUsage();
		}
		else if ( strcmp( argv[i], "-f2" ) )
		{
			if ( sscanf( argv[++i], "%s", &fileName2 ) != 1 )
				printUsage();
		}	
		else printUsage();
	}

	// ================================================================================================================
	
	// Get input image
	std::cout << "Loading image: " << fileName1 << std::endl;
	los::Heightmap input1( fileName1 );
	std::cout << "Image has " << input1.getWidth() << " x " << input1.getHeight() << " pixels" << std::endl;

	std::cout << "Loading image: " << fileName2 << std::endl;
	los::Heightmap input2( fileName2 );
	std::cout << "Image has " << input2.getWidth() << " x " << input2.getHeight() << " pixels" << std::endl;

	int error_count = 0;
	for ( int y = 0; y < input1.getHeight(); ++y ) 
	{
		for ( int x = 0; x < input1.getWidth(); ++x ) 
		{
			const uint8_t pixel1 = input1.getPixel(x, y);
			const uint8_t pixel2 = input2.getPixel(x, y);
			if (pixel1 != pixel2)  { 
				std::cout << "Error for pixel [" << x << ";" << y <<"]: " << std::endl;
				std::cout << "\t" << fileName1 << ": "	<< (int) pixel1 << std::endl;
				std::cout << "\t" << fileName2 << ": " << (int) pixel2 << std::endl;
				error_count++;
			}
		}
	}
	std::cout << "Number of errors: " << (int) error_count << std::endl;
}