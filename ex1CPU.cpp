#include <iostream>
#include <cstdint>
#include <cmath>

void CPU_viewtest(
    const int width,
    const int height,
    const int x_c,
    const int y_c,
    uint8_t *const pixel_in,
    uint8_t *const pixel_out
) {
    // Calculate height map value of pixel c
    int z_c = pixel_in[ x_c + y_c * width];

    // Examine every pixel for visibility check
    for (int i = 0; i < (height * width); ++i) {
        
        // The center pixel must be visible itself
        if (i == (x_c + y_c * width)) {
            pixel_out[i] = 255;
            continue;
        }

        // Calculate coordination of examining pixel - p
        int x_p = i % width;
        int y_p = i / width;
        int z_p = pixel_in[i];

        // Caculate angle from point c to pixel p
        float angle_p = atan((z_p - z_c)
                    /sqrt(pow(x_p - x_c, 2) + pow(y_p - y_c, 2)));

        // Find points for visibility check with DDA
        float dx = x_p - x_c;
        float dy = y_p - y_c;
        int step = std::max(abs(dx), abs(dy));

        float xIncrement = dx / step;
        float yIncrement = dy / step;

        int isVisible = 255;
        int j = 1;

        while (j < step) { 
            // Calculate coordination of examining pixel
            int x = x_c + j * xIncrement;
            int y = y_c + j * yIncrement;
            int z = pixel_in[ x + y * width ];

            // Calculate angle from pixel c to this pixel
            float angle = atan((z - z_c)
                    /sqrt(pow(x - x_c, 2) + pow(y - y_c, 2)));
            
            // Check visibility of pixel p
            if (angle >= angle_p) {
                isVisible = 0;
                break;
            }

            j++;
        }

        pixel_out[i] = isVisible;
    }
}