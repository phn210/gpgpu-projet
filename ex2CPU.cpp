#include <iostream>
#include <cstdint>
#include <cmath>

void CPU_tiled(
    const int width,
    const int height,
    const int finalWidth,
    const int finalHeight,
    const int tileWidth,
    const int tileHeight,
    uint8_t *const pixel_in,
    uint8_t *const pixel_out
) {
    //browse all the tiles in output image
    for (int xTile = 0; xTile < finalWidth; xTile++){
        for (int yTile = 0; yTile < finalHeight; yTile++){

            //reset height before processing the current tile
            int max_height = 0;

            //browse all the pixels of the current tile
            for (int i = xTile * tileWidth; i < ((xTile + 1) * tileWidth); i++){
                for (int j = yTile * tileHeight; j < ((yTile+1) * tileHeight); j++){

                    // Ensure the pixel index is within the bounds of the input image
                    if ((j + i * width) < width * height && max_height < pixel_in[j + i * width]){
                        max_height =  pixel_in[j + i * width];
                    }
                } 
            }

            //store max height of the current tile in the output
            pixel_out[yTile + xTile * finalWidth] = max_height;
        } 
    }
}
