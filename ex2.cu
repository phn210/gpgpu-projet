#include <iostream>
#include <cuda.h>
#include "ex2CPU.hpp"
#include "ex2GPU.hpp"
#include "utils/ppm.hpp"
#include "utils/chronoCPU.hpp"
#include "utils/chronoGPU.hpp"

void printUsage() 
{
	std::cerr	<< "Usage: " << std::endl
			<< " \t -f <F>: <F> image file name" 			
		    << std::endl << std::endl;
	exit( EXIT_FAILURE );
}

int main(int argc, char **argv) {
    // Load image
    char fileName[2048];

	// Parse program arguments
	if ( argc == 1 ) 
	{
		std::cerr << "Please give a file..." << std::endl;
		printUsage();
	}

	for ( int i = 1; i < argc; ++i ) 
	{
		if ( !strcmp( argv[i], "-f" ) ) 
		{
			if ( sscanf( argv[++i], "%s", &fileName ) != 1 )
				printUsage();
		}
		else
			printUsage();
	}

	// ================================================================================================================
	
	// Get input image
	std::cout << "Loading image: " << fileName << std::endl;
	los::Heightmap input( fileName );
	const int width		= input.getWidth();
	const int height	= input.getHeight();
	const int input_size  = width * height;
    const int finalWidth =10;
    const int finalHeight = 10;
	const int tileWidth = floor((float) width / finalWidth);
    const int tileHeight = floor((float) height / finalHeight);
	const int output_size = finalWidth * finalHeight;

	std::cout << "Image has " << width << " x " << height << " pixels" << std::endl;

	std::string baseSaveName = fileName;
	baseSaveName.erase( baseSaveName.end() - 9, baseSaveName.end() ); // erase .ppm

    // Define variables
	los::Heightmap outCPU( finalWidth, finalHeight );
	los::Heightmap outGPU1( finalWidth, finalHeight );
	los::Heightmap outGPU2( finalWidth, finalHeight );

	// Benchmark tools
	ChronoCPU chrCPU;
	ChronoGPU chrGPU;

    // ============================================================================================ //

	std::cout << "============================================"	<< std::endl;
	std::cout << "         Sequential version on CPU          "	<< std::endl;
	std::cout << "============================================"	<< std::endl;

	// Execute CPU function
	chrCPU.start();
	CPU_tiled( width, height, finalWidth, finalHeight, tileWidth, tileHeight, input.getPtr(), outCPU.getPtr());
	chrCPU.stop();

	std::cout << "-> Done : " << chrCPU.elapsedTime() << " ms" << std::endl;
	
	// ============================================================================================ //
	std::cout << "============================================"	<< std::endl;
	std::cout << "         Parallel version on GPU          "	<< std::endl;
	std::cout << "============================================"	<< std::endl;


    // Memory allocation
	uint8_t *dev_inPtr = new uint8_t[input_size];
	uint8_t *dev_outPtr1 = new uint8_t[output_size];
	uint8_t *dev_outPtr2 = new uint8_t[output_size];
	int *tmp_outPtr1 = new int[output_size];
	cudaMalloc(&dev_inPtr, input_size * sizeof(uint8_t));
	cudaMalloc(&dev_outPtr1, output_size * sizeof(uint8_t));
	cudaMalloc(&dev_outPtr2, output_size * sizeof(uint8_t));

    // Copy memory from host to device
	cudaMemcpy(dev_inPtr, input.getPtr(), input_size * sizeof(uint8_t), cudaMemcpyHostToDevice); 

    // Execute kernel function (GPU computation)
	// dim3 dimBlock(4, 4);
	// dim3 dimGrid((finalWidth + dimBlock.x - 1) / dimBlock.x, (finalHeight + dimBlock.y - 1) / dimBlock.y);
	chrGPU.start();
	GPU_naive_tiled<<<64, 64>>>(width, height, finalWidth, finalHeight, tileWidth, tileHeight, dev_inPtr, dev_outPtr1);
	chrGPU.stop();

	std::cout << "-> Done : " << chrGPU.elapsedTime() << " ms" << std::endl;

    // Copy memory from device to host
	cudaMemcpy(outGPU1.getPtr(), dev_outPtr1, output_size * sizeof(uint8_t), cudaMemcpyDeviceToHost);

	dim3 dimBlock(8,8);
	dim3 dimGrid(finalWidth, finalHeight);
	chrGPU.start();
	GPU_optimized_tiled<<<dimGrid, dimBlock>>>(width, height, finalWidth, finalHeight, (int)(width/finalWidth), (int)(height/finalHeight), dev_inPtr, dev_outPtr2);
	chrGPU.stop();

	std::cout << "-> Done : " << chrGPU.elapsedTime() << " ms" << std::endl;

    // Copy memory from device to host
	cudaMemcpy(outGPU2.getPtr(), dev_outPtr2, output_size * sizeof(uint8_t), cudaMemcpyDeviceToHost);
	
	// cudaError_t cudaError = cudaGetLastError();
	// if (cudaError != cudaSuccess) {
	// 	fprintf(stderr, "Erreur de transfert de mémoire : %s\n", cudaGetErrorString(cudaError));
	// }

    // Compare CPU and GPU computation
	std::cout << "============================================"	<< std::endl;
	std::cout << "              Checking results              "	<< std::endl;
	std::cout << "============================================"	<< std::endl;

	int error_count = 0;
	for ( int y = 0; y < finalHeight; ++y ) 
	{
		for ( int x = 0; x < finalWidth; ++x ) 
		{
			const uint8_t resCPU = outCPU.getPixel(x, y);
			const uint8_t resGPU = outGPU2.getPixel(x, y);
			if (resCPU != resGPU)  { 
				// std::cout << "Error for pixel [" << x << ";" << y <<"]: " << std::endl;
				// std::cout << "\t CPU: "	<< (int) resCPU << std::endl;
				// std::cout << "\t GPU: " << (int) resGPU << std::endl;
				error_count++;
				// exit( EXIT_FAILURE );
			}
		}
	}
	std::cout << "Number of errors with GPU : " << (int) error_count << std::endl;
	std::cout << "Congratulations! Job's done!" << std::endl << std::endl;

    // Export results
	std::string cpuName = baseSaveName + "output_tiled_CPU.ppm";
	outCPU.saveTo( cpuName.c_str() );

	std::string gpuName = baseSaveName + "output_tiled_GPU_naive.ppm";
	outGPU1.saveTo( gpuName.c_str() );

	std::string gpuName2 = baseSaveName + "output_tiled_GPU_optimized.ppm";
	outGPU2.saveTo( gpuName2.c_str() );

    // Free CPU and GPU memory
	cudaFree(dev_inPtr);
	cudaFree(dev_outPtr1);
	cudaFree(dev_outPtr2);
}