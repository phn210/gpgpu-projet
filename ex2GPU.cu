#include <iostream>
#include <cstdint>
#include <cmath>
#define threadPerBlock 64

__device__ void atomicMaxUint8(uint8_t *address, uint8_t val) {
    unsigned int *base_address = (unsigned int *)((size_t)address & ~3);

    unsigned int selectors[] = {0x3214, 0x3240, 0x3410, 0x4210};

    unsigned int sel = selectors[(size_t)address & 3];

    unsigned int old, assumed, max_, new_;

    old = *base_address;

    do {
        assumed = old;
        max_ = max(val, (uint8_t)__byte_perm(old, 0, ((size_t)address & 3)));
        new_ = __byte_perm(old, max_, sel);
        old = atomicCAS(base_address, assumed, new_);

    } while (assumed != old);
}


__global__ void GPU_naive_tiled(    
    const int width,
    const int height,
    const int finalWidth,
    const int finalHeight,
    const int tileWidth,
    const int tileHeight,
    uint8_t *const pixel_in,
    uint8_t *const pixel_out) {  

    // Calculate the overall index of thread in the grid
    int tid = threadIdx.x + blockIdx.x * blockDim.x;

    while (tid < width * height) {

        uint8_t z = pixel_in[tid];

        // Calculate coordination of examining pixel - p
        int x = (tid % width) / tileWidth;
        int y = (tid / width) / tileHeight;

        // Perform an atomicMax operation
        // to save the max value in the associated pixel
        if (x < finalWidth && y < finalHeight) {
            atomicMaxUint8(
                &pixel_out[x + y * finalWidth],
                z
            );
        } 

        tid += blockDim.x * gridDim.x;
    } 

}


__global__ void GPU_optimized_tiled(    
    const int width,
    const int height,
    const int finalWidth,
    const int finalHeight,
    const int tileWidth,
    const int tileHeight,
    uint8_t *const pixel_in,
    uint8_t *const pixel_out) {

    // Identify the current tile position in the output image
    int xTile = blockIdx.x;
    int yTile = blockIdx.y;
    
     // Shared memory to store maximum heights for each thread in the block
     //if threadPerBlock is wrong it can cause false result
    __shared__ uint32_t max_height_shared[threadPerBlock];

    // Calcul unique thread in 2D block and init it in max_height_shared
    int index_thread = threadIdx.x + blockDim.x * threadIdx.y;
    max_height_shared[index_thread] = 0;

    // Browse pixel of current tile
    // Each thread compares with multiple values within the tile
    for (int i = xTile * tileWidth + threadIdx.x ; i < ((xTile + 1) * tileWidth) && i < finalWidth * tileWidth; i+= blockDim.x){
        for (int j = yTile * tileHeight + threadIdx.y; j < ((yTile+1) * tileHeight) && j < finalHeight * tileHeight; j+=blockDim.y){
            atomicMax(&max_height_shared[index_thread], pixel_in[j + i * width]);
        }
    }

    // Wait for max_height_shared being fully update
    __syncthreads();

    // Only one thread make final comparaison
    if (threadIdx.x == 0 && threadIdx.y == 0) {
        int max_tile = max_height_shared[0];
        // Find maximum in max_height_shared with simple comparaison
        for (int k = 1; k < blockDim.y * blockDim.x; ++k) {
            max_tile = max(max_tile, max_height_shared[k]);
        }
        pixel_out[yTile + xTile * finalWidth] = (uint8_t)(max_tile);
    }
}

