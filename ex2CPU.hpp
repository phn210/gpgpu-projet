#ifndef __EX2CPU_HPP__
#define __EX2CPU_HPP__

void CPU_tiled(
    const int width,
    const int height,
    const int finalWidth,
    const int finalHeight,
    const int tileWidth,
    const int tileHeight,
    uint8_t *const pixel_in,
    uint8_t *const pixel_out
);

#endif // __EX2CPU_HPP__