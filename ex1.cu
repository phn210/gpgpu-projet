#include <iostream>
#include <cuda.h>
#include "ex1CPU.hpp"
#include "ex1GPU.hpp"
#include "utils/ppm.hpp"
#include "utils/chronoCPU.hpp"
#include "utils/chronoGPU.hpp"

// Print usage hint
void printUsage() 
{
	std::cerr	<< "Usage: " << std::endl
			<< " \t -f <F>: <F> image file name" 			
		    << std::endl << std::endl;
	exit( EXIT_FAILURE );
}

int main( int argc, char **argv) {
    // Load image
    char fileName[2048];

	// Parse program arguments to load input image
	if ( argc == 1 ) 
	{
		std::cerr << "Please give a file..." << std::endl;
		printUsage();
	}

	for ( int i = 1; i < argc; ++i ) 
	{
		if ( !strcmp( argv[i], "-f" ) ) 
		{
			if ( sscanf( argv[++i], "%s", &fileName ) != 1 )
				printUsage();
		}
		else
			printUsage();
	}
	
	/* ========== PREPARE INPUT ========== */
	
	// Load input image
	std::cout << "Loading image: " << fileName << std::endl;
	los::Heightmap input( fileName );
	const int width		= input.getWidth();
	const int height	= input.getHeight();
	const int img_size  = width * height;

	std::cout << "Image has " << width << " x " << height << " pixels" << std::endl;

	// Define output images
	los::Heightmap outCPU( width, height );
	los::Heightmap outGPU1( width, height );
	los::Heightmap outGPU2( width, height );
	los::Heightmap outGPU3( width, height );
	std::string baseSaveName = fileName;
	baseSaveName.erase( baseSaveName.end() - 9, baseSaveName.end() ); // erase input.ppm	

	// Benchmark tools
	ChronoCPU chrCPU;
	ChronoGPU chrGPU;

	// ========== CPU VERSION ========== //

	std::cout << "============================================"	<< std::endl;
	std::cout << "         Sequential version on CPU          "	<< std::endl;
	std::cout << "============================================"	<< std::endl;

	// Execute CPU function
	chrCPU.start();
	CPU_viewtest( width, height, 245, 497, input.getPtr(), outCPU.getPtr() );
	chrCPU.stop();

	// Print benchmark result
	std::cout << "-> Done (CPU): " << chrCPU.elapsedTime() << " ms" << std::endl;
	
	// ========== GPU VERSIONS ========== //

	std::cout << "============================================"	<< std::endl;
	std::cout << "          Parallel versions on GPU          "	<< std::endl;
	std::cout << "============================================"	<< std::endl;

    // Define variables
	uint8_t *dev_inPtr = new uint8_t[img_size];
	uint8_t *dev_out1Ptr = new uint8_t[img_size];
	uint8_t *dev_out2Ptr = new uint8_t[img_size];
	uint8_t *dev_out3Ptr = new uint8_t[img_size];
	float *dev_anglePtr = new float[img_size];
	// for (int i = 0; i < img_size; i++) {
	// 	dev_anglePtr[i] = (float) 0;
	// } 

	// Allocate memory for device
	cudaMalloc(&dev_inPtr, img_size * sizeof(uint8_t));
	cudaMalloc(&dev_out1Ptr, img_size * sizeof(uint8_t));
	cudaMalloc(&dev_out2Ptr, img_size * sizeof(uint8_t));
	cudaMalloc(&dev_out3Ptr, img_size * sizeof(uint8_t));
	cudaMalloc(&dev_anglePtr, img_size * sizeof(float));

    // Copy input data from host to device
	cudaMemcpy(dev_inPtr, input.getPtr(), img_size * sizeof(uint8_t), cudaMemcpyHostToDevice); 

    // Execute the naive GPU version
	chrGPU.start();
	GPU_naive_viewtest<<<256, 256>>>(width, height, 245, 497, dev_inPtr, dev_out1Ptr);
	chrGPU.stop();

	// Print benchmark result
	std::cout << "-> Done (GPU naive): " << chrGPU.elapsedTime() << " ms" << std::endl;

	// Copy memory from device to host
	cudaMemcpy(outGPU1.getPtr(), dev_out1Ptr, img_size * sizeof(uint8_t), cudaMemcpyDeviceToHost);

	// Execute the optimized GPU version
	chrGPU.start();
	GPU_optimized_viewtest_v2<<<256, 256>>>(width, height, 245, 497, dev_inPtr, dev_out3Ptr, dev_anglePtr);
	chrGPU.stop();

	// Print benchmark result
	std::cout << "-> Done (GPU optimized): " << chrGPU.elapsedTime() << " ms" << std::endl;

    // Copy memory from device to host
	cudaMemcpy(outGPU3.getPtr(), dev_out3Ptr, img_size * sizeof(uint8_t), cudaMemcpyDeviceToHost);
    
	// Execute the optimized GPU version
	chrGPU.start();
	GPU_optimized_viewtest_v1_part_1<<<256, 256>>>(width, height, 245, 497, dev_inPtr, dev_anglePtr);
	GPU_optimized_viewtest_v1_part_2<<<256, 256>>>(width, height, 245, 497, dev_inPtr, dev_out2Ptr, dev_anglePtr);
	chrGPU.stop();
	
	// Print benchmark result
	std::cout << "-> Done (GPU optimized): " << chrGPU.elapsedTime() << " ms" << std::endl;

    // Copy memory from device to host
	cudaMemcpy(outGPU2.getPtr(), dev_out2Ptr, img_size * sizeof(uint8_t), cudaMemcpyDeviceToHost);
     
	std::cout << "Congratulations! Job's done!" << std::endl << std::endl;

    // Export results
	std::string cpuName = baseSaveName + "output_CPU.ppm";
	outCPU.saveTo( cpuName.c_str() );
	std::string gpuName1 = baseSaveName + "output_naive_GPU.ppm";
	outGPU1.saveTo( gpuName1.c_str() );
	std::string gpuName2 = baseSaveName + "output_optimized_v1_GPU.ppm";
	outGPU2.saveTo( gpuName2.c_str() );
	std::string gpuName3 = baseSaveName + "output_optimized_v2_GPU.ppm";
	outGPU3.saveTo( gpuName3.c_str() );

	// Free GPU memory
	cudaFree(dev_inPtr);
	cudaFree(dev_out1Ptr);
	cudaFree(dev_out2Ptr);
	cudaFree(dev_anglePtr);
	
	// Free CPU memory
	// delete[] dev_inPtr;
	// delete[] dev_out1Ptr;
	// delete[] dev_out2Ptr;
	// delete[] dev_anglePtr;
}